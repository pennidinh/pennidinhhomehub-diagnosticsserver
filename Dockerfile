FROM debian:stretch

RUN apt-get update && apt-get --yes install python-pip
RUN pip install glances
RUN pip install bottle

COPY daemon.sh /app/daemon.sh
RUN chmod 777 /app/daemon.sh

CMD glances -w -p 80 --fahrenheit
